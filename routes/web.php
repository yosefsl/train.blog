<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
    Route::get('/hello', function () {
        return "welcome";
});
Route::get('/customers/{id?}', function ($id=null) {
   
   if($id==null)
    return "you need to register";
   else
    return 'Hello customer number '.$id;
});

route::get('/comment/{id}',function ($id){
    return view('comment',['id'=>$id]);
})->name('comments');